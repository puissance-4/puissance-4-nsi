def victoire(gril, j):
    '''Fonction victoire(gril,j):
    Renvoie un boléen True si le joueur j a gagné, False sinon.
    Fait appel aux fonctions horiz(), vert(), diag_haut(), et diag_bas()'''
    for i in range(5):
        for c in range(6):
            if horiz(gril,j,i,c) == True:
                return True
            if vert(gril,j,i,c) == True:
                return True
            if diag_haut(gril,j,i,c) == True:
                return True
            if diag_bas(gril,j,i,c) == True:
                return True
    return False