def diag_bas(gril , j , lig , col):
    '''Fonction diag_bas(gril, j, lig, col):
    Détermine s'il y a un alignement diagonal vers le bas
    4 pions du joueur j à partir de la case (lig,col).
    Arguments:
    gril la grille avec les pions.
    j le joueur, un entier avec la valeur 1 ou 2
    lig la ligne, un entier avec la valeur entre 0 et 6
    Renvoie True si c'est le cas, False sinon.'''
    if gril[lig][col] == j and gril[lig+1][col+1] == j and gril[lig+2][col+2] == j and gril[lig+3][col+3] == j:
        return True
    else:
        return False
assert diag_bas([[0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0, 0],
         [0, 0, 1, 0, 2, 0, 0],
         [0, 0, 1, 0, 2, 0, 0],
         [0, 0, 1, 0, 2, 0, 0]],1,0,2) == False
assert diag_bas([[1, 0, 1, 1, 1, 1, 1],
         [0, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0, 0],
         [0, 0, 1, 0, 2, 0, 0],
         [0, 0, 1, 0, 2, 0, 0],
         [0, 0, 1, 0, 2, 0, 0]],1,0,2) == False
assert diag_bas([[1, 0, 1, 1, 1, 1, 1],
             [0, 1, 0, 0, 0, 0, 0],
             [0, 0, 1, 0, 0, 0, 0],
             [0, 0, 1, 1, 2, 0, 0],
             [0, 0, 1, 0, 2, 0, 0],
             [0, 0, 1, 0, 2, 0, 0]],1,0,0) == True