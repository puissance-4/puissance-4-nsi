def coup_aleatoire(gril,j):
    '''Fonction coup_aleatoire(gril,j):
    Joue un coup aléatoire pour le joueur j.
    On suppose la grille non pleine,
    condition indispensable pour ne pas se trouver dans une boucle infinie !
    '''
    from random import randint
    i = randint(0,6)
    if match_nul(gril) == False and gril[0][i] == 0:
        jouer(gril,j,i)
    return gril