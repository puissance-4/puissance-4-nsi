def jouer(gril,j,col):
    '''
    Fonction jouer(gril,j,col):
    Fonction qui joue un coup du joueur j dans le colonne col de la grille.
    Arguments :
        gril est la grille de 6 x 7 avec les pions des joueurs
        j est un entier qui a la valeur 1 ou 2 suivant le joueur
        col est un entier entre 0 et 6, désigne une colonne non pleine de la grille
    Si j vaut 1 la première case vide de la colonne col prendra la valeur 1
    Si j vaut 2 la première case vide de la colonne col prendra la valeur 2
    '''
    ligne=[6,5,4,3,2,1,0]
    for i in ligne:
        if gril[i][col]==0:
            if j==1:
                gril[i][col]=1
                return gril
            if j==2:
                gril[i][col]=2
                return gril
assert jouer([[0,0,0,0,1,0],
            [0,1,2,2,1,0],
            [0,1,1,2,2,0],
            [0,2,2,1,0,1],
            [0,0,0,0,0,0],
            [0,1,2,2,1,0],
            [0,1,2,1,2,1]],1,0) == [[0,0,0,0,1,0],
                                    [0,1,2,2,1,0],
                                    [0,1,1,2,2,0],
                                    [0,2,2,1,0,1],
                                    [0,0,0,0,0,0],
                                    [0,1,2,2,1,0],
                                    [1,1,2,1,2,1]]
assert jouer([[0,0,0,0,1,0],
            [0,1,2,2,1,0],
            [0,1,1,2,2,0],
            [0,2,2,1,0,1],
            [0,0,0,0,0,0],
            [0,1,2,2,1,0],
            [0,1,2,1,2,1]],2,0) == [[0,0,0,0,1,0],
                                    [0,1,2,2,1,0],
                                    [0,1,1,2,2,0],
                                    [0,2,2,1,0,1],
                                    [0,0,0,0,0,0],
                                    [0,1,2,2,1,0],
                                    [2,1,2,1,2,1]]
assert jouer([[0,0,0,0,1,0],
            [0,1,2,2,1,0],
            [0,1,1,2,2,0],
            [0,2,2,1,0,1],
            [0,1,0,0,0,0],
            [0,1,2,2,1,0],
            [0,1,2,1,2,1]],1,1) == [[0,1,0,0,1,0],
                                    [0,1,2,2,1,0],
                                    [0,1,1,2,2,0],
                                    [0,2,2,1,0,1],
                                    [0,1,0,0,0,0],
                                    [0,1,2,2,1,0],
                                    [0,1,2,1,2,1]]



