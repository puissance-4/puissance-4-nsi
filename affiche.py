def affiche(gril):
    '''Fonction affiche (gril): affiche une grille de 6 lignes sur 7 colonnes.
    La fonction prend en argument un tableau de taille de 6x7.
    Une ligne est notée lig et prend une valeur entre 0 et 5,
    la ligne 0 est stiuée en bas.
    Une colonne est notée col et prend une valeur entre 0 et 6,
    la colonne 0 est située à gauche.
    Dans la grille:
    la valeur 0 représente une case vide, représentée par un.
    La valeur 1 représente un pion du joueur 1, représenté par un x.
    la valeur 2 représente un pion du joeur 2, représenté par un 0.'''
    for i in range(6):
        for z in range(7):
            if gril[i][z] == 0:
                print("|.",end='')
            elif gril[i][z] == 1:
                print("|x", end= '')
            else:
                print("|0",end='')