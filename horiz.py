def horiz (gril,j,lig):
    """
    Fonction horiz(gril, j, lig)
    Détermine s'il y a un alignement horizontale de 4 pions du joueur j
    à partir de la case (lig)
    arguments:
        gril la grille avec les pions
        j le joueur, un entier avec la valeur 1 ou 2
        lig la ligne, un entier avec la valeur entre 0 et 5
    Renvoie True si c'est le cas
    """
    tabl=gril
    m=0
    if tabl[lig][0]==j and tabl[lig][1]==j and tabl[lig][2]==j and tabl[lig][3]==j:
        return True
    else :
        return False
    if tabl[lig][1]==j and tabl[lig][2]==j and tabl[lig][3]==j and tabl[lig][4]==j:
        return True
    else:
        return False
    if tabl[lig][2]==j and tabl[lig][3]==j and tabl[lig][4]==j and tabl[lig][5]==j:
        return True
    else:
        return False
    if tabl[lig][3]==j and tabl[lig][4]==j and tabl[lig][5]==j and tabl[lig][6]==j:
        return True
    else:
        return False     

assert horiz([[0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [1, 1, 1, 2, 1, 0, 0]],1,5)==False
assert horiz([[0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [2, 2, 2, 2, 0, 0, 0]],2,5)==True
