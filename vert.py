def vert (gril,j,lig,col):
    """
    Fonction vert(gril, j, lig, col):
    Détermine s'il y a un alignement vertical de 4 pions du joueurs j
    à partir de la case (lig,col).
    arguments:
        grill la grille avec les pions
        j le joueur, un entier avec la valeur 1 ou 2
        lig la ligne, un entier avec la valeur entre 0 et 2
        col la colonne, un entier avec la valeur entre 0 et 6
    Renvoie True si c'est le cas
    """
    tabl=gril
    if tabl[lig][col]==j and tabl[lig+1][col]==j and tabl[lig+2][col]==j and tabl[lig+3][col]==j:
        return True
    else :
        return False

assert vert([[0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [1, 0, 0, 0, 0, 0, 0],
 [1, 0, 0, 0, 0, 0, 0],
 [1, 0, 0, 0, 0, 0, 0]],1,2,0)==False
assert vert([[0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [2, 0, 0, 0, 0, 0, 0],
 [2, 0, 0, 0, 0, 0, 0],
 [2, 0, 0, 0, 0, 0, 0],
 [2, 0, 0, 0, 0, 0, 0]],2,2,0)==True
