def match_nul(gril):
    '''
    Fonction match_nul(gril):
    Renvoie True si la partie est nulle,
    c'est à dire si la ligne du haut est remplie,
    False sinon.
    '''
    if 0 in gril[0]:
        return False
    return True

assert match_nul([[0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0, 0],
                [0, 0, 1, 0, 2, 0, 0],
                [0, 0, 1, 0, 2, 0, 0],
                [0, 0, 1, 0, 2, 0, 0]]) == False
assert match_nul([[1, 1, 1, 1, 1, 1, 1],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0, 0],
                [0, 0, 1, 0, 2, 0, 0],
                [0, 0, 1, 0, 2, 0, 0],
                [0, 0, 1, 0, 2, 0, 0]]) == True
assert match_nul([[1, 0, 1, 1, 1, 1, 1],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0, 0],
                [0, 0, 1, 0, 2, 0, 0],
                [0, 0, 1, 0, 2, 0, 0],
                [0, 0, 1, 0, 2, 0, 0]]) == False
