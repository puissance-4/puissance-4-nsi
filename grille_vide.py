def grille_vide():
    '''
    Fonction grille_vide() :
    La fonction construit un tableau à deux dimensions de taille 6 x 7 :
    6 lignes et 7 colonnes.
    Chaque case contient la valeur 0
    La fonction ne prend pas d'argument
    La fonction renvoie le tableau
    '''
    grille = [] #créer le tableau 
    ligne = [] #créer les lignes
    for i in range(7) :
        ligne.append(0) #mettre 7 fois la valeur 0 dans une ligne
    for j in range (6) :
        grille.append(ligne) #mettre 6 fois une ligne dans la grille
    return grille

    for i in range(7) :
        ligne.append(0) #mettre 7 fois la valeur 0 dans une ligne
    for j in range (6) :
        grille.append(ligne) #mettre 6 fois une ligne dans la grille
    return grille



    
assert grille_vide() == [[0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0]]
assert len(grille_vide()) == 6
​
