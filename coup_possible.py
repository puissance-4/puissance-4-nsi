def coup_posssible(gril, col) :
    '''
    Fonction coup_possible(gril, col):
    Determine si possible de jouer dans la colonne col.
    Prend en argument la grille, tableau de 6 x 7, avec la positions des pionts
    des joueurs et un entier, le numéro de colonne entre 0 et 6.
    Renvoie True si possible de jouer dans la colonne col, False sinon.
    Il est possible de jouer dans la colonne col, s'il existe une case avec
    la valeur 0 dans cette colonne. 
    '''
    if gril[0][col] == 0:
        return True
    return False

assert coup_posssible([[1,1,2,2,2,1,2],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [1,2,2,0,0,0,0]],1) == False
assert coup_posssible([[0,1,2,2,2,1,2],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [1,2,2,0,0,0,0]],0) == True
assert coup_posssible([[1,1,2,2,0,1,2],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [1,2,2,0,0,0,0]],4) == True
assert coup_posssible([[1,1,2,2,0,1,2],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0],
                       [1,2,2,0,0,0,0]],3) == False